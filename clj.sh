apt-get install -qq curl
repo="https://download.clojure.org/install/"
script="linux-install-1.11.1.1208.sh"
curl -O ${repo}${script}
chmod +x $script
./$script
version="2023.01.26-11.08.16"
bin=/usr/local/bin/clojure-lsp
wget -O $bin https://github.com/clojure-lsp/clojure-lsp/releases/download/${version}/clojure-lsp
chmod +x $bin
apt-get install -qq leiningen

repo="https://github.com/babashka/babashka/releases/download"
version="1.2.174"
arch="linux-amd64"
tar="${repo}/v${version}/babashka-${version}-${arch}.tar.gz"
wget -q -O- "${tar}" | tar -xz -C "/usr/local/bin"
