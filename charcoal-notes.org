
* charcoal
:PROPERTIES:
:org-remark-file: ~/dev/charcoal.clj
:END:

** touch!
:PROPERTIES:
:org-remark-beg: 421
:org-remark-end: 427
:org-remark-id: 270d2c57
:org-remark-label: green
:org-remark-link: [[file:~/dev/charcoal.clj::12]]
:END:
- [[https://github.com/IgnorantGuru/udevil/pull/106/commits/7f19f932a76417f7f8f063b41c6f61a9d0bd7558][workaround]] :: spire's ~line-in-file~ doesn't work on empty files, or files that don't exist. This ensures a usable file exists.

** line-in-file
:PROPERTIES:
:org-remark-beg: 803
:org-remark-end: 815
:org-remark-id: 270d2c56
:org-remark-label: green
:org-remark-link: [[file:~/dev/charcoal.clj::25]]
:END:
[[https://github.com/IgnorantGuru/udevil/pull/106/commits/7f19f932a76417f7f8f063b41c6f61a9d0bd7558][workaround]] to mount exfat with udevil
