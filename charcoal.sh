user=kk

### Git
apt-get install -qq git-lfs git-crypt

apt-get install -qq redshift
cp redshift.conf /home/$user/redshift.conf

### Audio driver
sudo apt install firmware-sof-signed pavucontrol

### Emacs
apt-get install -qq texinfo libxpm-dev libjpeg-dev libgif-dev libtiff-dev libgnutls28-dev libncurses-dev
bash ./emacs.sh
bash ./kimacs.sh

### Element Chat
sudo apt install -y wget apt-transport-https

sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list

sudo apt update

sudo apt install element-desktop

bash js.sh

### LaTeX
apt-get install -qq texlive-base texlive-latex-extra

### Bitwig
apt-get install -qq ffmpeg libxcb-icccm4 libxcb-xinput0 libxcb-xkb1 libxkbcommon-x11-0 libxcb-ewmh2
wget https://www.bitwig.com/dl/?id=462&os=installer_linux
dpkg -i bitwig-studio-4.0.8.deb

bash ./git-delta.sh

### kk/git
apt-get install -qq devmon
sudo -u $user tee -a /home/${user}/.bash_profile <<EOF > /dev/null
devmon --exec-on-drive "notify-send 'hi' 'hi'"
EOF

# bash ./startx.sh

bb charcoal.clj
