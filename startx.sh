### start x server on login
### This should be at the very end of the .bash_profile script.
sudo -u $user tee -a $bash_profile <<EOF > /dev/null
if [ -z "\${DISPLAY}" ] && [ "\${XDG_VTNR}" -eq 1 ]; then
   exec startx
fi
EOF
