(ns spire-test
  (:require [babashka.pods :as pods]))

(pods/load-pod 'epiccastle/spire "0.1.2")

(require '[pod.epiccastle.spire.selmer :refer [selmer]]
         '[pod.epiccastle.spire.module.upload :refer [upload]]
         '[pod.epiccastle.spire.module.line-in-file :refer [line-in-file]]
         '[pod.epiccastle.spire.module.shell :refer [shell]]
         '[pod.epiccastle.spire.module.sudo :refer [sudo]])

(defn touch! [filename]
  (shell {:cmd (str "if [ ! -s " filename " ]; then "
                    "touch " filename "; "
                    "echo '' >> " filename "; fi;")}))

(def user "kk")
(def home (str "/home/" user))
(def bash-profile (str home "/.bash_profile"))
(touch! bash-profile)
(def xinitrc (str home "/.xinitrc"))
(touch! xinitrc)

(defn project-sync-on-mount []
  (sudo (line-in-file :present
                      {:path "/etc/udevil/udevil.conf"
                       :string-match "default_options_exfat"
                       :line (str "default_options_exfat     = "
                                  "nosuid, noexec, nodev, noatime, "
                                  "iocharset=utf8, namecase=0")}))

  (line-in-file :present
                {:path xinitrc
                 :insert-at :bof
                 :string-match "devmon"
                 :line "devmon --exec-on-drive \"notify-send 'hi' 'You just mounted a drive.'\" &"}))

#_(project-sync-on-mount)
