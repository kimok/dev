export DEBIAN_FRONTEND=noninteractive
apt-get -qq update

### user
user=me
pass=developer
home=/home/$user
passfile=/home/vagrant/passfile
bashrc=/home/$user/.bashrc
bash_profile=/home/$user/.bash_profile
adduser -gecos "" --disabled-password $user
chpasswd <<<"${user}:${pass}"
usermod -aG sudo $user
echo ". .bashrc" | sudo -u $user tee -a $bash_profile

apt-get install -qq git
sudo -u $user git config --global user.name $user
sudo -u $user git config --global user.email ${user}@example.com

### shared folder
share=/home/${user}/dev
mkdir -p $share
mount --bind /vagrant $share
mount -o remount,uid=$(id -u $user),gid=$(id -u $user) $share

### auto login
service_d="/etc/systemd/system/getty@tty1.service.d"
override=${service_d}/override.conf
mkdir -p $service_d
echo "[Service]"    > $override
echo "Type=simple" >> $override
echo "ExecStart="  >> $override
echo "ExecStart=-/sbin/agetty --autologin $user --noclear %I 38400 linux" >> $override

bash emacs.sh

### hack ligature font
fonts_dir=/home/${user}/.local/share/fonts
sudo -u $user mkdir -p ${fonts_dir}
git clone https://github.com/pyrho/hack-font-ligature-nerd-font.git
cp hack-font-ligature-nerd-font/font/* ${fonts_dir}
rm -r hack-font-ligature-nerd-font
chown -R $user ${fonts_dir}
echo "fc-cache -f"
sudo -u $user fc-cache -f

### tuntox
repo="https://github.com/gjedeer/tuntox/releases/download/0.0.10/"
bin="tuntox-x64"
asc="tuntox-x64.asc"
### TODO: add keybase ip to proxy branch
curl https://keybase.io/gdr/pgp_keys.asc | gpg --import

wget ${repo}${bin}
#chmod +x $bin
#mv $bin /usr/local/bin/

bash ./git-delta.sh
bash ./kimacs.sh

### exwm
apt-get install -qq xorg x11-xserver-utils
sudo -u $user tee -a /home/${user}/.xinitrc <<EOF > /dev/null
[ -f /etc/xprofile ] && . /etc/xprofile
[ -f ~/.xprofile ] && . ~/.xprofile
exec emacs --exwm
EOF
sudo -u $user tee -a /home/${user}/.xprofile <<EOF > /dev/null
xset r rate 180 40
xrandr --newmode "1920x1080_60.00" 173.00 1920 2048 2248 2576 1080 1083 1088 1120 -hsync +vsync
xrandr --addmode Virtual1 1920x1080_60.00

xrandr --newmode "1000x1000_60.00" 83.00 1000 1064 1168 1336 1000 1003 1013 1038 -hsync +vsync
xrandr --addmode Virtual1 1000x1000_60.00

xrandr --newmode "2560x1440_60.00"  312.25  2560 2752 3024 3488  1440 1443 1448 1493 -hsync +vsync
xrandr --addmode Virtual1 2560x1440_60.00

xrandr -s 1920x1080_60.00
EOF
bash startx.sh
### desktop programs
apt-get install -qq firefox-esr

### graalvm ###
apt-get install -qq wget
url=https://github.com/graalvm/graalvm-ce-builds/releases/download/
version=22.3.0
prefix=graalvm-ce-java19
arch=linux-amd64
archive=${prefix}-${arch}-${version}.tar.gz
target=/usr/local/share
graal_home=${target}/graalvm
rm -rf $graal_home
wget -q ${url}vm-${version}/${archive}
mkdir -p $target
tar -xzf $archive
mv ${prefix}-${version} $graal_home
rm $archive
sudo -u $user tee -a $bashrc <<EOF
export PATH=${graal_home}/bin:\$PATH
export JAVA_HOME=$graal_home
export GRAALVM_HOME=$graal_home
EOF

### llvm on graalvm ###
${graal_home}/bin/gu install llvm llvm-toolchain
echo "export LLVM_TOOLCHAIN=\$(\$JAVA_HOME/bin/lli --print-toolchain-path)" >> $bashrc

### programming languages
bash clj.sh
bash js.sh
bash cpp.sh

### bash ###
npm i -g bash-language-server

### projects
project_remote=ash
apt-get -qq install nfs-common git-lfs

### kimok/edaw
apt-get -qq install libgstreamer1.0-dev
tee -a /etc/fstab <<EOF
${project_remote}:/edaw ${home}/edaw nfs defaults
${project_remote}:/wf ${home}/wf nfs defaults
EOF

bash ./proj/cv.sh
