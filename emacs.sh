### emacs
if ! command -v emacs &> /dev/null
then
    apt-get install -qq git build-essential autoconf libgtk-3-dev libwebkit2gtk-4.0-dev
    apt-get build-dep -qq emacs
    if [ ! -d "./emacs" ]; then
    	git clone --shallow-since 2023-01-20 https://github.com/emacs-mirror/emacs.git
    fi
    cd emacs
    git checkout f4a3e8f29f05f19263d3f600823cdbc0b1cfd3ef
    ./autogen.sh
    ./configure --with-cairo --with-xwidgets --with-x-toolkit=gtk3
    make
    make install
    cd .. && rm -r emacs
fi

