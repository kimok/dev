user=${user-me}
config=${config-/home/$user/.config}
kimacs=${kimacs-$config/emacs}

apt-get install -qq cmake libtool libtool-bin ripgrep fonts-noto-color-emoji
sudo -u $user mkdir -p $config
git clone --branch vagrant https://gitlab.com/kimok/kimacs.git $kimacs
chown -R $user $kimacs
chmod -R a+rw $kimacs
sudo -u $user bash $kimacs/install.sh
wait $!
echo "alias kimacs=emacs" >> $bashrc
echo "alias kmcs=emacs"   >> $bashrc
